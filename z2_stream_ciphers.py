def lfsr(key, data):
    data = list(data)
    state = list(key)
    cipher = []

    for i in data:
        # Last bit of the LFSR is the keystream byte
        keystream_byte = state[-0]

        # XOR the plaintext byte with the keystream byte
        cipher_byte = i ^ keystream_byte
        cipher.append(cipher_byte)

        # Update the LFSR state
        # XOR feedback from specific bits
        feedback = state[0]
        for i in range(len(state)-1):
            feedback = feedback ^ state[i+1]
        
        # Shift the LFSR state and append feedback
        state = [feedback] + state[:-1] 

    return bytes(cipher)


# EXAMPLE
key = [1, 0, 1, 0, 1]  # LFSR initial state
plaintext = b"Hello, World!"
encrypted = lfsr(key, plaintext)
print("Encrypted:", encrypted.decode("utf-8"))

decrypted = lfsr(key, encrypted)
print("Decrypted:", decrypted.decode("utf-8"))

plaintext = b"Adam Zalewski"
encrypted = lfsr(key, plaintext)
print("Encrypted:", encrypted.decode("utf-8"))

decrypted = lfsr(key, encrypted)
print("Decrypted:", decrypted.decode("utf-8"))
