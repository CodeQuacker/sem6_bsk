def matrix2a(key, input, validation):

    print("input: " + str(input))

    key = [int(digit) for digit in str(key)]
    
    # CIPHER
    rowLength = len(key)
    row = 0
    indices = []
    ciphered = ""

    # put all letters in array
    for index, letter in enumerate(input):
        indices.append(letter)

    # while there are any truthy indice go row by row
    # and in each row take letters in order of key
    while any(indices):
        for col in key:
            try:
                ciphered += "".join(indices[(col - 1) + (rowLength * row)])
                indices[(col - 1) + (rowLength * row)] = None
            except:
                continue
        row += 1

    # print result with check
    print("CIPHER   - correct: " 
            + str(ciphered == validation) 
            + ", ciphered: " 
            + str(ciphered) 
            + ", validation: " 
            + str(validation))
    
    # DECIPHER
    row = 0
    index = 0
    deciphered = ""

    # using previously emptied table put all letters back
    # into their spot
    while not all(indices):
        for col in key:
            try:
                indices[(col - 1) + (rowLength * row)] = ciphered[index]
                index += 1
            except:
                continue
        row += 1

    # join table to string
    for letter in indices: deciphered += "".join(letter)
    
    # print result with check
    print("DECIPHER - correct: " 
            + str(deciphered == input) 
            + ", deciphered: " 
            + str(deciphered) 
            + ", validation: " 
            + str(input))
    print()


def matrix2b(key, input, validation):

    print("input: " + str(input))

    # KEY TRANSLATION
    # make 2 tables of key, one normal, one sorted
    # using them build key that represents order 
    # in which columns should be taken
    keyIndices = []
    sortedKeyIndices = []
    for keyLetter in key:
        keyIndices.append(keyLetter)
    sortedKeyIndices = sorted(keyIndices)

    key = ""

    for indice in sortedKeyIndices:
        index = keyIndices.index(indice)
        key += str(index+1)
        keyIndices[index] = None

    key = [int(digit) for digit in str(key)]


    # CIPHER
    rowLength = len(key)
    indices = []
    ciphered = ""

    # put all letters in array
    for index, letter in enumerate(input):
        indices.append(letter)
    rowCount = int(len(indices)/rowLength) + 1

    # while there are any truthy indice go row by row
    # and in each row take letters in order of key
    while any(indices):
        for col in key:
            for row in range(rowCount):
                try:
                    ciphered += "".join(indices[(col - 1) + (rowLength * row)])
                    indices[(col - 1) + (rowLength * row)] = None
                except:
                    continue

    # print result with check
    print("CIPHER   - correct: " 
            + str(ciphered == validation) 
            + ", ciphered: " 
            + str(ciphered) 
            + ", validation: " 
            + str(validation))
    
    # DECIPHER
    index = 0
    deciphered = ""

    # using previously emptied table put all letters back
    # into their spot
    while not all(indices):
        for col in key:
            for row in range(rowCount):
                try:
                    indices[(col - 1) + (rowLength * row)] = ciphered[index]
                    index += 1
                except:
                    continue

    # join table to string
    for letter in indices: deciphered += "".join(letter)

    # print result with check    
    print("DECIPHER - correct: " 
            + str(deciphered == input) 
            + ", deciphered: " 
            + str(deciphered) 
            + ", validation: " 
            + str(input))
    print()



print("==========================")
print("matrix 2a for key = 34152")
print("==========================")
matrix2a(34152, "FRANCJA", "ANFCRJA")
matrix2a(34152, "DOBRYDZIEN", "BRDYOIEDNZ")
matrix2a(34152, "POLITECHNIKA", "LIPTOHNEICKA")
matrix2a(34152, "UNIVERSITY", "IVUENITRYS")
print("=========================")
print("matrix 2b for key = BCADA")
print("=========================")
matrix2b("BCADA", "FRANCJA", "ACFJRAN")
matrix2b("BCADA", "DOBRYDZIEN", "BIYNDDOZRE")
matrix2b("BCADA", "POLITECHNIKA", "LHTIPEKOCAIN")
matrix2b("BCADA", "UNIVERSITY", "IIEYURNSVT")
